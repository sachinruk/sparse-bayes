clear all
close all
clc

%test file for structured sparsity
% rng(1);
N=20;
x=zeros(100,1);
Phi=randn(N,100);
idx=randperm(100);
ne=zeros(100,2);
noise=1;
%choose first 2 indices and have a 5 set clump
for i=1:2
    if idx(i)<3
        x(idx(i):(idx(i)+4))=randn(5,1);
    elseif idx(i)>98
        x((idx(i)-4):idx(i))=randn(5,1);
    else
        x((idx(i)-2):(idx(i)+2))=randn(5,1);
    end    
end

for i=1:100
    ne(i,1)=i+1; ne(i,2)=i-1;
    if i==100
        ne(i,1)=0;
    elseif i==1
        ne(i,2)=0;
    end    
end


y=Phi*x+noise*randn(N,1);

figure; plot(x)
% mu=vbMixture2(y,Phi,ne);
[mu2, Ez]=vbMixture3(y,Phi); mu2(Ez<0.5)=0;
[mu3, Ez2]=spike_n_slab(y,Phi); mu3(Ez2<0.5)=0;
muRVM=variationalRVM(y,Phi);
mul2=Phi\y;
hold on; plot(mu2,'k');
plot(mu3,'r');
figure; plot(x); hold on; plot(muRVM,'g');
% plot(mul2,'k');
disp([mean((x-mu2).^2) mean((x-mu3).^2) mean((x-muRVM).^2) mean((x-mul2).^2)]);
disp([mean(abs(x-mu2)) mean(abs(x-mu3))  mean(abs(x-muRVM)) mean(abs(x-mul2))]);