function val=U(a,nz)

z=-nz; arg=0.25*z^2; nu=0.25; nu2=0.75;

U0=sqrt(0.5*z/pi)*besselk(nu,arg);

psi_1_4=(besseli(nu,arg)+besseli(-nu,arg))/cos(nu*pi);
V0=0.5*sqrt(0.5*z)*psi_1_4;

U1=(1/sqrt(2*pi))*z^1.5*(besselk(nu2,arg)-besselk(nu,arg));

psi_3_4=(besseli(nu2,arg)+besseli(-nu2,arg))/cos(nu2*pi);
V1=0.5*(0.5*z)^1.5*(psi_1_4-psi_3_4);

U_0nz=-sin(pi*a)*U0+sqrt(pi)*V0;
U_1nz=-sin(pi*a)*U1+2*sqrt(pi)*V1;

fracU=U_1nz/U_0nz;
E1=(1+z*fracU);
E1_2=fracU;
val=[E1 E1_2];