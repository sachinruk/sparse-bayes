clear all
close all
clc

sigma2=2;
y=randn*2;
c=[1 3; 5 3];

theta1=-3:0.1:3;
theta2=-3:0.1:3;

[T1, T2]=meshgrid(theta1,theta2);
theta=[T1(:) T2(:)];
A=[1 0;0 0.5];
w=randn(2,1);

ln_theta=zeros(length(theta),1);
for i=1:length(theta)
    sum_kernel=zeros(1,2);
    for j=1:2
        sum_kernel(j)=exp(-theta(i,:).^2*c(:,j));
    end
    ln_theta(i)=-0.5*(theta(i,:)*A*theta(i,:)'+(y-sum_kernel*w)^2/sigma2);
end

ln_theta_r=reshape(ln_theta,size(T1));

surf(T1,T2,ln_theta_r);

figure; surf(T1,T2,log(ln_theta_r-min(ln_theta)));

x=-5:0.1:5;
y1=-0.5*(y^2-2*w(1)*y*exp(-c(1)*x.^2)+w(1)^2*exp(-2*c(1)*x.^2));
figure; plot(x,exp(y1-max(y1))); 
hold on; plot(x,w(1)*y*exp(-c(1)*x.^2),'r');
plot(x,-0.5*w(1)^2*exp(-2*c(1)*x.^2),'g'); hold off;