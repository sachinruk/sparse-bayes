function t=trace2(A,B)
%trace of square SYMMETRIC matrices
t=sum(A(:).*B(:));