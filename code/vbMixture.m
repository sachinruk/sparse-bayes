function mu=vbMixture(y,Phi,Ne)

Ni=sum(Ne>0,2);
k=1;

while 1
    [mu,Sigma]=qw(y,PhiTPhi,Ez,Eln_pi,EtauInv,Eainv);
    dSigma=diag(Sigma);
    Ez=qz(mu,dSigma,Eln_pi,Eainv);
    Eln_pi=qpi(Ez,Ni,mu,dSigma);
    EtauInv=qtau(y,Phi,mu,Sigma);
    Eainv=qa(Ez,mu,dSigma);
    k=k+1;
    if norm(mu-muOld)<1e-5 && k>10
        break;
    end
    muOld=mu; k=k+1;
end

function [mu,Sigma]=qw(y,Phi,Ez,Eln_pi,Etau,Ea)
sigma2=1e-3;
A=Ez.*Ea+(1-Ez)/1000;
PhiTPhi=Etau*(Phi'*Phi);
L=chol(diagSum(PhiTPhi,A),'lower'); dSigma=diag(Sigma);
mu=Etau*(L'\(L\(Phi'*y)));
ln1_pi=Eln_pi(:,2);
C=sqrt(2*pi*sigma2)*sum(ln1_pi(Ni),2);
norm=exp(-0.5*(log(2*pi*(sigma2+dSigma))+mu.^2./(sigma2+dSigma)));
%adjust for dSigma=0
frac1=(mu.^2-dSigma-sigma2)./((sigma2+dSigma).^2);
frac2=-1./(sigma2+dSigma);
Lambda1=C.*norm.*frac1;
Lambda2=C.*norm.*frac2;
Sigma=chol(diagSum(PhiTPhi,A+Lambda1),'lower');
L=chol(diagSum(PhiTPhi,A+Lambda2),'lower');
mu=L'\(L\Etau*Phi'*y);


function Ez=qz(Ew2,Eln_pi,Ea)

% Ew2=mu.^2+dSigma;
logp_q=Eln_pi(:,1)-Eln_pi(:,2)-0.5*Ew2*(Ea-1/1000)+0.5*(Elna+log(1000));
Ez=1./(1+exp(-logp_q));

function Eln_pi=qpi(Ec,Ni,mu,dSigma)
alpha=Ec+Ni+1;
beta=2-Ec+2*Ni;%+E(exp(-w^2/2sigma2))
lnpi=psi(alpha)-psi(alpha+beta);
ln1_pi=psi(beta)-psi(alpha+beta);
Eln_pi=[lnpi ln1_pi];

function EtauInv=qtau(y,Phi,mu,Sigma)
a=0.5*length(y)+1e-9;
b=0.5*(norm(y-Phi*mu)+trace2(Sigma,(Phi'*Phi)))+1e-9;
EtauInv=a/b;

function Ea=qa(Ez,Ew2)
a=0.5+1e-9;
b=0.5*Ez.*Ew2+1e-9;
Ea=a./b;