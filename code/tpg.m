clear all
close all
clc

load tpg.mat

mu_p=mean(tpg_price);
tpg_price=tpg_price-mu_p;
tpg_price=flip(tpg_price);
t=1:length(tpg_price); t=t';

figure; plot(tpg_price);

%linear part
Phi_lin=[ones(length(t),1) t t.^2];
%sinusoidal part
% hold on; plot(t,sin(t/10),'k')
PhiSin=[];
for i=10:40
    PhiSin=[PhiSin sin(2*pi*t/i) cos(2*pi*t/i)];
end

Phi=[Phi_lin PhiSin];


beta=regress(log(tpg_price+5), [ones(length(t),1) t]);
figure(1); hold on; plot(t, exp(beta(1))*exp(beta(2)*t)-5,'g')
tpg_res=tpg_price-exp(beta(1))*exp(beta(2)*t)+5;
[Sigma,mu,A,lambda]=variational(tpg_res,Phi);
figure; plot(t,tpg_res);
y2=zeros(size(Phi(:,1)));
idx=find(abs(mu)>1e-3);
for i=1:length(idx)
    y2=y2+mu(idx(i))*Phi(:,idx(i));
end
hold on; plot(t,y2,'r')
hold on; plot(t,Phi(:,4),'g')
