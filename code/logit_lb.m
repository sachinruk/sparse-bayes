clear all
close all
clc

z=-100:0.1:10000;
u=0:0.1:1000;
sigma2=1;

overall_p=zeros(size(z));
for i=1:length(u)
    ln_p=-0.5*(log(2*pi*sigma2)+(z-u(i)).^2);
    p=exp(ln_p)/u(end);
    overall_p=overall_p+p;
end

figure; plot(z,overall_p);