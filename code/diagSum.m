function sqrA=diagSum(sqrA,diag)

n=size(sqrA,1);
sqrA(1:(n+1):end)=sqrA(1:(n+1):end)+diag';