clear all
close all
clc

x=audioread('bird.wav');
Phi=dftmtx(1024);

y=zeros(1024,100);i=1; k=1;
while ((k+1024)<length(x))
    y(:,i)=Phi*x(k:(k+1023));
    i=i+1;
    k=k+1024;
end

figure;spectrogram(x);
figure; imagesc(real(y'))