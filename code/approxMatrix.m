clear all
close all
clc

addpath('~/thesis/GP/gpml-matlab-v3.2-2013-01-15')
startup
x=0:0.1:200; x=x';
hyp=[log(1) log(1)];
K=covSEard(hyp,x);
[U, D]=svd(K); d=diag(D);