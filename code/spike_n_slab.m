function [mu, Ez]=spike_n_slab(y,Phi)

k=1;
%initial estimates
PhiTy=Phi'*y;
PhiTPhi=(Phi'*Phi);
Ez=0.5*ones(size(Phi,2),1);
Etau=var(y)*0.5;
Ef=randn(size(Phi,2),1);
Sigmaii=10*rand(size(Phi,2),1);
while k<1000
    [mu,Ew2,Sigma]=qw(PhiTy,PhiTPhi,Ez,Etau);
    Ez=qz(Ew2);
    Etau=qtau(y,Phi,PhiTPhi,mu,Sigma);
    
    
    if k>1
        if norm(mu-muOld)<1e-5 && k>50
            
            break;
        end
        diff(k-1)=norm(mu-muOld);
    end
    muOld=mu; k=k+1;
end
disp(k);

function [mu,Ew2,Sigma]=qw(PhiTy,PhiTPhi,Ez,Etau)
A=(1-Ez)*10000+Ez/1000;
SigmaInv=diagSum(Etau*PhiTPhi,A);
L=chol(SigmaInv,'lower'); 
Sigma=L'\(L\eye(size(SigmaInv)));
% mu=Etau*(L'\(L\(Phi'*y)));
mu=Etau*(Sigma*PhiTy);
Ew2=diag(Sigma)+mu.^2;

function Ez=qz(Ew2)
% logp=-0.5*(log(2*pi)*Es0(:,2)+Es2(:,2)-2*Es(:,2).*Ef+Ef2.*Es0(:,2)+log(1000)+Ew2/1000);
% logq=-0.5*(log(2*pi)*Es0(:,1)+Es2(:,1)-2*Es(:,1).*Ef+Ef2.*Es0(:,1)-log(10000)+Ew2*10000);
logp=log(0.1)-0.5*(log(1000)+Ew2/1000);
logq=log(0.9)-0.5*(-log(10000)+Ew2*10000);
% logp_q=logp-logq;
% Ez=1./(1+exp(-logp_q));
Ez=1./(1+exp(logq-logp));

function Etau=qtau(y,Phi,PhiTPhi,mu,Sigma)
a=0.5*length(y)+1e-3;
b=0.5*(norm(y-Phi*mu)+trace2(Sigma,PhiTPhi))+1e-3;
Etau=a/b;

% function [Ef,Ef2,Sigmaii]=qf(Ez,Es0,Es)
% A=Es0(:,1).*(1-Ez)+Es0(:,2).*Ez;
% K=covPPiso(0,[log(2) 0],[1:length(Ez)]');
% % K=eye(length(Ez));
% L=chol(K,'lower');
% l=length(K);
% K(1:(l+1):end)=K(1:(l+1):end)+1./A';
% L2=chol(K,'lower');
% 
% % Sigmaest=L*(eye()-cholInv'*cholInv)*L';
% cholInv=L2\L;
% Sigma=-cholInv'*cholInv;
% Sigma(1:(l+1):end)=Sigma(1:(l+1):end)+1;
% Sigma=L*Sigma*L';
% 
% a=Es(:,1).*(1-Ez)+Es(:,2).*Ez-L'\(L\(2*ones(length(Es),1)));
% Ef=Sigma*a;
% Sigmaii=diag(Sigma);
% Ef2=Sigmaii+Ef.^2;
% 
% function [Es0,Es,Es2]=qs(Ef,Sigmaii,Ez)
% C1=exp(-0.5*(1-Ez).*Sigmaii+0.5*log(2*pi./(1-Ez))-0.5*(1-Ez)*log(2*pi));
% C2=exp(-0.5*Ez.*Sigmaii+0.5*log(2*pi./Ez)-0.5*Ez*log(2*pi));
% C_left=C1.*normcdf(-Ef.*sqrt(1-Ez));
% C_right=C2.*normcdf(Ef.*sqrt(Ez));
% C=C_left+C_right; %normalising constant
% Es0=[C_left./C C_right./C];
% 
% %mean
% Es_left=C1.*(Ef.*normcdf(-Ef.*sqrt(1-Ez))-normpdf(Ef.*sqrt(1-Ez))./(1-Ez));
% Es_right=C2.*(Ef.*normcdf(Ef.*sqrt(Ez))+normpdf(Ef.*sqrt(Ez))./Ez);
% Es=[Es_left./C Es_right./C];
% 
% %second moment
% Es2_left=C1.*(normcdf(-Ef.*sqrt(1-Ez)).*(Ef.^2+1./(1-Ez))-normpdf(Ef.*sqrt(1-Ez)).*Ef./(1-Ez));
% Es2_right=C2.*(normcdf(Ef.*sqrt(Ez)).*(Ef.^2+1./Ez)+normpdf(Ef.*sqrt(Ez)).*Ef./Ez);
% Es2=[Es2_left./C Es2_right./C];
% 
% %adjustments if Ez=1 or 0
% z_0_idx=Ez<eps;
% z_1_idx=Ez>(1-eps);
% mu_z0=Ef(z_0_idx); mu_z1=Ef(z_1_idx);
% Es0(z_0_idx,1)=normcdf(-mu_z0); Es0(z_0_idx,2)=0;
% Es0(z_1_idx,2)=normcdf(mu_z1); Es0(z_1_idx,1)=0;
% Es(z_0_idx,1)=-normpdf(mu_z0)./normcdf(-mu_z0)+mu_z0; Es(z_0_idx,2)=0;
% Es(z_1_idx,2)=normpdf(mu_z1)./normcdf(mu_z1)+mu_z1; Es(z_1_idx,1)=0;
% Es2(z_0_idx,1)=-mu_z0.*normpdf(mu_z0)./normcdf(-mu_z0)+mu_z0.^2+1; Es2(z_0_idx,2)=0;
% Es2(z_1_idx,2)=mu_z1.*normpdf(mu_z1)./normcdf(mu_z1)+mu_z1.^2+1; Es2(z_1_idx,1)=0;