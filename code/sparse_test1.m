clear all
close all
clc

addpath('../Convex Optimisation/cvx/');
cvx_setup;
Phi=randn(30,100);
w=randn(100,1);
idx=randperm(100);
r_idx=idx(1:10);
w(idx(11:end))=0;
y=Phi*w+0.5*randn(30,1);

n=100;
cvx_begin
    variable mu3(n)
    minimize( norm(Phi*mu3-y)+1*norm(mu3,1));
cvx_end

[Sigma,mu,A,lambda]=variational(y,Phi);
[Sigma2,mu2,A2]=variationalRVM(y,Phi);
figure; 
subplot(131); title('Adaptive Lasso');
plot(mu); hold on; plot(r_idx,mu(r_idx),'rx'); 
plot(r_idx,w(r_idx),'gx'); hold off;
subplot(132); title('RVM');
plot(mu2); hold on; plot(r_idx,mu2(r_idx),'rx'); 
plot(r_idx,w(r_idx),'gx'); hold off;
subplot(133); title('cvx');
plot(mu3); hold on; plot(r_idx,mu3(r_idx),'rx'); 
plot(r_idx,w(r_idx),'gx'); hold off;

%metrics
disp([sum(abs(mu-w)) sum(abs(mu2-w)) sum(abs(mu3-w))]);
disp([sum((mu-w).^2) sum((mu2-w).^2) sum((mu3-w).^2)]);