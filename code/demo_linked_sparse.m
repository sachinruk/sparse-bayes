clear all
close all
clc

u=randn(10000,1);
% v=randn(10000,1);

% b=sqrt(u.^2+v.^2).*randn(10000,1);
c=u.*randn(10000,1);
figure; hist(c,100);
[n, x]=hist(c,100);
figure;plot(sign(x).*x.^2,-log(n))

%2D example
Phi=[1 0.9;0.9 1];
L=chol(Phi)';
u=L*randn(2,100000);
v=L*randn(2,100000);
b=sqrt(u.^2+v.^2).*randn(2,100000);
[N,C]=hist3(b',[100,100]);
[x, y]=meshgrid(C{1,1},C{1,2});
% figure; surf(x,y,N)
figure; surf(x,y,log(N))

Phi=[1 0;0 1];
L=chol(Phi)';
u=L*randn(2,100000);
v=L*randn(2,100000);
b=sqrt(u.^2+v.^2).*randn(2,100000);
[N,C]=hist3(b',[100,100]);
[x, y]=meshgrid(C{1,1},C{1,2});
% figure; surf(x,y,N)
figure; surf(x,y,log(N))

Phi=[1 0;0 1];
L=chol(Phi)';
u=L*randn(2,100000);
v=L*randn(2,100000);
b=(u+v).*randn(2,100000);
[N,C]=hist3(b',[100,100]);
[x, y]=meshgrid(C{1,1},C{1,2});
% figure; surf(x,y,N)
figure; surf(x,y,log(N))

Phi=[1 0.2;0.2 1];
L=chol(Phi)';
u=L*randn(2,100000);
v=L*randn(2,100000);
b=(u+v).*randn(2,100000);
[N,C]=hist3(b',[100,100]);
[x, y]=meshgrid(C{1,1},C{1,2});
% figure; surf(x,y,N)
figure; surf(x,y,log(N))

%same as above with only 1 u
Phi=[1 0.9;0.9 1];
L=chol(Phi)';
u=L*randn(2,100000);
b=u.*randn(2,100000);
[N,C]=hist3(b',[100,100]);
[x, y]=meshgrid(C{1,1},C{1,2});
% figure; surf(x,y,N)
figure; surf(x,y,log(N))

Phi=[1 0;0 1];
L=chol(Phi)';
u=L*randn(2,100000);
b=u.*randn(2,100000);
[N,C]=hist3(b',[100,100]);
[x, y]=meshgrid(C{1,1},C{1,2});
% figure; surf(x,y,N)
figure; surf(x,y,log(N))