function mu=vbMixture2(y,Phi,Ne)

k=1;
%initial estimates
PhiTy=Phi'*y;
PhiTPhi=(Phi'*Phi);
Ez=0.5*ones(size(Phi,2),1);
Etau=var(y)*0.5;
Ea=zeros(size(Ez))+1e6; Elna=zeros(size(Ez))+log(1e6);
Epi=Ez;
Eln1_pi=-Ez;
while 1
    [mu,Ew2,Sigma]=qw(PhiTy,PhiTPhi,Ez,Etau,Ea);
%     [Ea, Elna]=qa(Ez,Ew2);
    [Epi,Elnpi, Eln1_pi]=qpi(Ez,Ne,Epi,Eln1_pi);
    Ez=qz(Ew2,Elnpi,Eln1_pi,Ea,Elna);
    Etau=qtau(y,Phi,PhiTPhi,mu,Sigma);    
    if k>1
        if norm(mu-muOld)<1e-10 && k>10
            disp(k);
            break;
        end
    end
    muOld=mu; k=k+1;
end

function [mu,Ew2,Sigma]=qw(PhiTy,PhiTPhi,Ez,Etau,Ea)
A=Ez.*Ea+(1-Ez)/1000;
SigmaInv=diagSum(Etau*PhiTPhi,A);
L=chol(SigmaInv,'lower'); 
Sigma=L'\(L\eye(size(SigmaInv)));
% mu=Etau*(L'\(L\(Phi'*y)));
mu=Etau*(Sigma*PhiTy);
Ew2=diag(Sigma)+mu.^2;

function Ez=qz(Ew2,Elnpi,Eln1_pi,Ea,Elna)
logp_q=Elnpi-Eln1_pi-0.5*Ew2.*(Ea-1/1000)+0.5*(Elna+log(1000));
Ez=1./(1+exp(-logp_q));


function [Epi,Elnpi,Eln1_pi]=qpi(Ez,Ne,Epi,Eln1_pi)
[Ni, Epi_neighbours, Eln1_pi_neighbours]=findNeighbours(Ne,Epi,Eln1_pi);
alpha=Ez+Ni+2;
beta=3-Ez+Ni-Epi_neighbours;
T=10; Ci=-Eln1_pi_neighbours;
facts=factorial(0:T);
Epi=zeros(size(Eln1_pi));
Elnpi=zeros(size(Eln1_pi));
Eln1_pi=zeros(size(Eln1_pi));
NC=zeros(size(Eln1_pi));
for i=0:T
    const=Ci.^i/facts(i+1);
    gamma_ratio=exp(gammaln(alpha+i)-gammaln(alpha+beta+i));
    NC=NC+const.*gamma_ratio;
    Epi=Epi+const.*gamma_ratio.*(alpha+i)./(alpha+beta+i);
    Elnpi=Elnpi+const.*(psi(alpha+i)-psi(alpha+beta+i)).*gamma_ratio;
    Eln1_pi=Eln1_pi+const.*(psi(beta)-psi(alpha+beta+i)).*gamma_ratio;
end
Epi=Epi./NC;
Elnpi=Elnpi./NC;
Eln1_pi=Eln1_pi./NC;

function Etau=qtau(y,Phi,PhiTPhi,mu,Sigma)
a=0.5*length(y)+1e-9;
b=0.5*(norm(y-Phi*mu)+trace2(Sigma,PhiTPhi))+1e-9;
Etau=a/b;

function [Ea, Elna]=qa(Ez,Ew2)
a=0.5+1e-9;
b=0.5*Ez.*Ew2+1e-9;
Ea=a./b;
Elna=psi(a)-log(b);

function [Ni, Epi_neighbours, Eln1_pi_neighbours]=findNeighbours(Ne,Epi,Eln1_pi)
Ni=sum(Ne>0,2);
mask=Ne>0; Ne(Ne==0)=1;
Epi_neighbours=sum(Epi(Ne).*mask,2);
Eln1_pi_neighbours=sum(Eln1_pi(Ne).*mask,2);