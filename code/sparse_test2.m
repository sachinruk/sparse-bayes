clear all
close all
clc
% 
% addpath('../Convex Optimisation/cvx/');
% cvx_setup;
for i=1:25
    Phi=randn(30,100);
    w=randn(100,1);
    idx=randperm(100);
    r_idx=idx(1:10);
    w(idx(11:end))=0;
    y=Phi*w+0.5*randn(30,1);

    [Sigma,mu,A,lambda]=variational(y,Phi);
    [Sigma2,mu2,A2]=variationalRVM(y,Phi);
    % figure; 
    % subplot(131); title('Adaptive Lasso');
    % plot(mu); hold on; plot(r_idx,mu(r_idx),'rx'); 
    % plot(r_idx,w(r_idx),'gx'); hold off;
    % subplot(132); title('RVM');
    % plot(mu2); hold on; plot(r_idx,mu2(r_idx),'rx'); 
    % plot(r_idx,w(r_idx),'gx'); hold off;
    % subplot(133); title('cvx');
    % plot(mu3); hold on; plot(r_idx,mu3(r_idx),'rx'); 
    % plot(r_idx,w(r_idx),'gx'); hold off;

    %metrics
    ALassoMAD(i)=mean(abs(mu-w)); RVMMAD(i)=mean(abs(mu2-w)); 
    ALassoRMSE(i)=mean((mu-w).^2); RVMRMSE(i)=mean((mu2-w).^2);
    disp(i);
end

sum(ALassoMAD<RVMMAD)
sum(ALassoRMSE<RVMRMSE)
disp([mean(ALassoMAD) mean(RVMMAD) mean(ALassoRMSE) mean(RVMRMSE)]);